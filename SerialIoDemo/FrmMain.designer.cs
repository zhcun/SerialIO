﻿
namespace SerialIoDemo
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rtbServerLogSrc = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rtbServerLog = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnServerStart = new System.Windows.Forms.Button();
            this.coBoxServerPort = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rtbClientLogSrc = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rtbClientLog = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtData = new System.Windows.Forms.TextBox();
            this.btnWriteText = new System.Windows.Forms.Button();
            this.btnWriteHex = new System.Windows.Forms.Button();
            this.btnClientStart = new System.Windows.Forms.Button();
            this.coBoxClientPort = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.btnSendSrc = new System.Windows.Forms.Button();
            this.txtSrcData = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.splitter2);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(656, 749);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "上位机(Server)模拟";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.rtbServerLogSrc);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(4, 599);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(648, 145);
            this.panel4.TabIndex = 3;
            // 
            // rtbServerLogSrc
            // 
            this.rtbServerLogSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbServerLogSrc.Location = new System.Drawing.Point(4, 29);
            this.rtbServerLogSrc.Name = "rtbServerLogSrc";
            this.rtbServerLogSrc.Size = new System.Drawing.Size(637, 108);
            this.rtbServerLogSrc.TabIndex = 1;
            this.rtbServerLogSrc.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "原始日志：";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(4, 596);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(648, 3);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.rtbServerLog);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(4, 82);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(648, 514);
            this.panel3.TabIndex = 2;
            // 
            // rtbServerLog
            // 
            this.rtbServerLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbServerLog.Location = new System.Drawing.Point(4, 29);
            this.rtbServerLog.Name = "rtbServerLog";
            this.rtbServerLog.Size = new System.Drawing.Size(637, 477);
            this.rtbServerLog.TabIndex = 1;
            this.rtbServerLog.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "解析日志：";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnServerStart);
            this.panel1.Controls.Add(this.coBoxServerPort);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(4, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 58);
            this.panel1.TabIndex = 1;
            // 
            // btnServerStart
            // 
            this.btnServerStart.Location = new System.Drawing.Point(199, 10);
            this.btnServerStart.Name = "btnServerStart";
            this.btnServerStart.Size = new System.Drawing.Size(92, 32);
            this.btnServerStart.TabIndex = 2;
            this.btnServerStart.Text = "打开";
            this.btnServerStart.UseVisualStyleBackColor = true;
            this.btnServerStart.Click += new System.EventHandler(this.btnServerStart_Click);
            // 
            // coBoxServerPort
            // 
            this.coBoxServerPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxServerPort.FormattingEnabled = true;
            this.coBoxServerPort.Location = new System.Drawing.Point(72, 12);
            this.coBoxServerPort.Name = "coBoxServerPort";
            this.coBoxServerPort.Size = new System.Drawing.Size(121, 28);
            this.coBoxServerPort.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "串口：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel6);
            this.groupBox2.Controls.Add(this.splitter3);
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(660, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(826, 749);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "下位机（Client）模拟";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.rtbClientLogSrc);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(4, 639);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(818, 105);
            this.panel6.TabIndex = 6;
            // 
            // rtbClientLogSrc
            // 
            this.rtbClientLogSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbClientLogSrc.Location = new System.Drawing.Point(4, 29);
            this.rtbClientLogSrc.Name = "rtbClientLogSrc";
            this.rtbClientLogSrc.Size = new System.Drawing.Size(807, 68);
            this.rtbClientLogSrc.TabIndex = 1;
            this.rtbClientLogSrc.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "原始日志：";
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(4, 636);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(818, 3);
            this.splitter3.TabIndex = 5;
            this.splitter3.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.rtbClientLog);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(4, 122);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(818, 514);
            this.panel5.TabIndex = 3;
            // 
            // rtbClientLog
            // 
            this.rtbClientLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbClientLog.Location = new System.Drawing.Point(4, 29);
            this.rtbClientLog.Name = "rtbClientLog";
            this.rtbClientLog.Size = new System.Drawing.Size(807, 477);
            this.rtbClientLog.TabIndex = 1;
            this.rtbClientLog.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "解析日志：";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.txtSrcData);
            this.panel2.Controls.Add(this.btnSendSrc);
            this.panel2.Controls.Add(this.txtData);
            this.panel2.Controls.Add(this.btnWriteText);
            this.panel2.Controls.Add(this.btnWriteHex);
            this.panel2.Controls.Add(this.btnClientStart);
            this.panel2.Controls.Add(this.coBoxClientPort);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(4, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(818, 98);
            this.panel2.TabIndex = 2;
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(298, 12);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(234, 26);
            this.txtData.TabIndex = 4;
            // 
            // btnWriteText
            // 
            this.btnWriteText.Location = new System.Drawing.Point(636, 8);
            this.btnWriteText.Name = "btnWriteText";
            this.btnWriteText.Size = new System.Drawing.Size(92, 32);
            this.btnWriteText.TabIndex = 3;
            this.btnWriteText.Text = "WriteText";
            this.btnWriteText.UseVisualStyleBackColor = true;
            this.btnWriteText.Click += new System.EventHandler(this.btnWriteText_Click);
            // 
            // btnWriteHex
            // 
            this.btnWriteHex.Location = new System.Drawing.Point(538, 8);
            this.btnWriteHex.Name = "btnWriteHex";
            this.btnWriteHex.Size = new System.Drawing.Size(92, 32);
            this.btnWriteHex.TabIndex = 3;
            this.btnWriteHex.Text = "WriteHex";
            this.btnWriteHex.UseVisualStyleBackColor = true;
            this.btnWriteHex.Click += new System.EventHandler(this.btnWriteHex_Click);
            // 
            // btnClientStart
            // 
            this.btnClientStart.Location = new System.Drawing.Point(199, 10);
            this.btnClientStart.Name = "btnClientStart";
            this.btnClientStart.Size = new System.Drawing.Size(92, 32);
            this.btnClientStart.TabIndex = 2;
            this.btnClientStart.Text = "打开";
            this.btnClientStart.UseVisualStyleBackColor = true;
            this.btnClientStart.Click += new System.EventHandler(this.btnClientStart_Click);
            // 
            // coBoxClientPort
            // 
            this.coBoxClientPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxClientPort.FormattingEnabled = true;
            this.coBoxClientPort.Location = new System.Drawing.Point(72, 12);
            this.coBoxClientPort.Name = "coBoxClientPort";
            this.coBoxClientPort.Size = new System.Drawing.Size(121, 28);
            this.coBoxClientPort.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "串口：";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(656, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 749);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // btnSendSrc
            // 
            this.btnSendSrc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendSrc.Location = new System.Drawing.Point(733, 49);
            this.btnSendSrc.Name = "btnSendSrc";
            this.btnSendSrc.Size = new System.Drawing.Size(75, 30);
            this.btnSendSrc.TabIndex = 5;
            this.btnSendSrc.Text = "发送";
            this.btnSendSrc.UseVisualStyleBackColor = true;
            this.btnSendSrc.Click += new System.EventHandler(this.btnSendSrc_Click);
            // 
            // txtSrcData
            // 
            this.txtSrcData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSrcData.Location = new System.Drawing.Point(72, 52);
            this.txtSrcData.Name = "txtSrcData";
            this.txtSrcData.Size = new System.Drawing.Size(655, 26);
            this.txtSrcData.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Data：";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1486, 749);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "固定头示例（2021.9.29）";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox coBoxServerPort;
        private System.Windows.Forms.Button btnServerStart;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnClientStart;
        private System.Windows.Forms.ComboBox coBoxClientPort;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbServerLog;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.RichTextBox rtbServerLogSrc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RichTextBox rtbClientLog;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RichTextBox rtbClientLogSrc;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Button btnWriteHex;
        private System.Windows.Forms.Button btnWriteText;
        private System.Windows.Forms.TextBox txtSrcData;
        private System.Windows.Forms.Button btnSendSrc;
        private System.Windows.Forms.Label label7;
    }
}

