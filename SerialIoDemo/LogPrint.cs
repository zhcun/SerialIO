﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialIoDemo
{
    class LogPrint
    {
        public static Action<string> PrintHandle;

        public static void Print(string log)
        {
            PrintHandle?.Invoke(log);
        }
    }
}
