﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.SerialIO.Filters;

namespace SerialIoDemo.FixedHead
{
    public class FHDemoFilter : FixedHeadFilter
    {
        static byte[] Head = new byte[] { 0xaa, 0xbb };

        public FHDemoFilter()
            : base(Head, 6)
        { }

        //[0xaa,0xbb] [len] [cmd] [DevId] [data] [crc-1,crc-2]

        protected override int GetDataLength(byte[] data, int offset, int count)
        {
            //数据包长度 第3个字节
            return data[offset + 2];
        }

        protected override int GetPackageKey(byte[] data, int offset, int count)
        {
            //命令字 第4个字节
            return data[offset + 3];
        }
    }
}