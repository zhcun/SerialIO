﻿固定头的协议示例代码

协议格式：[2字节 固定头 0xaa,0xbb] + [1字节 长度 len] [1字节 命令字] [1字节 设备号] [N字节 data] [2字节 CRC校验码]
PS： data长度 N = len - 7

下位机下发指令：
1. data为16进制数值
2. data为字符串内容

程序步骤：
	1. 创建接收过滤器 FHDemoFilter 继承于 FixedHeadFilter
	2. 创建接收的命令处理对象 CmdHex CmdText
	3. 创建服务类 FDServer 将 FHDemoFilter 与 CmdHex、CmdText 加入到服务中
	4. 创建协议对应的 接收数据Model R_Hex,R_Text 与 发送数据Model W_Hex,W_Text