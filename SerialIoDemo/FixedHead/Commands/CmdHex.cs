﻿using SerialIoDemo.FixedHead.ProtocolModel;
using ZhCun.SerialIO;

namespace SerialIoDemo.FixedHead.Commands
{
    public class CmdHex : CmdBase<R_Hex>
    {
        public override int Key => 0x01;

        public override string CmdName => "Hex";

        public override void ExecuteHandle(ISerialServer server, R_Hex rep)
        {
            //收到hex数据；
            LogPrint.Print($"[HEX]: {rep.Hex}");

            //回复消息
            W_HexRe w = new W_HexRe();
            w.DevId = rep.DevId;
            server.Write(w);                        
        }
    }
}