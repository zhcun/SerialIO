﻿using SerialIoDemo.FixedHead.ProtocolModel;
using ZhCun.SerialIO;

namespace SerialIoDemo.FixedHead.Commands
{
    public abstract class CmdBase<TReadModel> : ICommand
        where TReadModel : R_Base, new()
    {
        public abstract int Key { get; }

        public abstract string CmdName { get; }

        /// <summary>
        /// 执行响应逻辑
        /// </summary>
        public abstract void ExecuteHandle(ISerialServer server, TReadModel rep);

        public virtual void Execute(ISerialServer server, PackageInfo package)
        {
            var rModel = new TReadModel();
            var r = rModel.Analy(package.Body, package.BodyOffset, package.BodyCount);
            if (r == 0)
            {
                ExecuteHandle(server, rModel);
            }
            else
            {
                LogPrint.Print($"解析key={Key} 异常，error code: {r}");
            }
        }
    }
}
