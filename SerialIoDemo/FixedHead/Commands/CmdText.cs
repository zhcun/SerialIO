﻿using SerialIoDemo.FixedHead.ProtocolModel;
using ZhCun.SerialIO;

namespace SerialIoDemo.FixedHead.Commands
{
    public class CmdText : CmdBase<R_Text>
    {
        public override int Key => 0x02;

        public override string CmdName => "Text";

        public override void ExecuteHandle(ISerialServer server, R_Text rep)
        {
            LogPrint.Print($"[Text]: {rep.Text}");
            //回复消息
            var w = new W_TextRe();
            w.DevId = rep.DevId;
            server.Write(w);

            //.. to do something 
        }
    }
}