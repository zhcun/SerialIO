﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.SerialIO;

namespace SerialIoDemo.FixedHead
{
    public class FDServer : SerialServerBase
    {
        protected override void LoadCommands()
        {
            LoadCommands("SerialIoDemo", s => s.Namespace.Equals("SerialIoDemo.FixedHead.Commands"));
        }

        protected override void LoadFilters()
        {
            Filters.Add(new FHDemoFilter());
        }
    }
}