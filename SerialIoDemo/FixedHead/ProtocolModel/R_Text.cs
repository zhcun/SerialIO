﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    public class R_Text : R_Base
    {
        public string Text { set; get; }

        protected override int AnalyBody(BytesAnaly analy)
        {
            Text = analy.GetString(analy.NotAnalyCount - 2);
            return 0;
        }
    }
}
