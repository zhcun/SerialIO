﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    public class W_Text : W_Base
    {
        protected override byte CmdWord => 0x02;

        public string Text { set; get; }

        protected override void SetBodyData(BytesEncode encode)
        {
            encode.AddString(Text);
        }
    }
}