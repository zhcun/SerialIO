﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    public class R_Hex : R_Base
    {
        public string Hex { set; get; }

        protected override int AnalyBody(BytesAnaly analy)
        {
            Hex = analy.GetHexString(analy.NotAnalyCount - 2);
            return 0;
        }
    }
}
