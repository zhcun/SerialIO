﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    public class W_TextRe : W_Base
    {
        protected override byte CmdWord => 0x02;

        protected override void SetBodyData(BytesEncode encode)
        {
            encode.AddByte(0xEE);
        }
    }
}
