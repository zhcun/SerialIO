﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    public class W_Hex : W_Base
    {
        protected override byte CmdWord => 0x01;

        public string Hex { set; get; }

        protected override void SetBodyData(BytesEncode encode)
        {
            encode.AddHex(Hex);
        }
    }
}
