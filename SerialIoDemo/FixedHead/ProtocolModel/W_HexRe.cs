﻿using ZhCun.SerialIO.Utils;

namespace SerialIoDemo.FixedHead.ProtocolModel
{
    /// <summary>
    /// 收到Hex后的回复
    /// </summary>
    public class W_HexRe : W_Base
    {
        protected override byte CmdWord => 0x01;

        protected override void SetBodyData(BytesEncode encode)
        {
            encode.AddByte(0xEE);
        }
    }
}
