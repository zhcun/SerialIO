﻿/*
    创建日期: 2018.6.14
    创建者:张存
    邮箱:zhangcunliang@126.com
    说明：用于协议编码的帮助类
    修改记录:
        注册 CodePagesEncodingProvider

 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace ZhCun.SerialIO.Utils
{
    /// <summary>
    /// 字节数组编码帮助类
    /// </summary>
    public class BytesEncode
    {
        static BytesEncode()
        {
#if NETSTANDARD
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
#endif
        }
        /// <summary>
        /// 初始化
        /// </summary>
        public BytesEncode()
        {
            _ReaultList = new List<byte>();
        }

        /// <summary>
        /// 用于默认转换字符串的编码(默认为UTF-8)
        /// </summary>
        public Encoding TextEncoding { set; get; } = Encoding.UTF8;

        List<byte> _ReaultList;
        /// <summary>
        /// 获取已添加的数据长度
        /// </summary>
        public int GetResultCount()
        {
            return _ReaultList.Count;
        }

        /// <summary>
        /// 获取结果列表
        /// </summary>
        public List<byte> GetResult()
        {
            return _ReaultList;
        }
        /// <summary>
        /// 清除结果
        /// </summary>
        public void ClearData()
        {
            _ReaultList.Clear();
        }
        /// <summary>
        /// 增加一个空(0)字节
        /// </summary>
        /// <param name="count">要增加的字节数</param>
        public BytesEncode AddEmptyBytes(int count)
        {
            for (int i = 0; i < count; i++)
                AddByte(0);
            return this;
        }
        /// <summary>
        /// 增加集合
        /// </summary>
        public BytesEncode AddBytes(IEnumerable<byte> data)
        {
            _ReaultList.AddRange(data);
            return this;
        }
        /// <summary>
        /// 增加一个字节数组
        /// </summary>
        public BytesEncode AddByte(byte data)
        {
            _ReaultList.Add(data);
            return this;
        }
        /// <summary>
        /// 增加一组字节数组，指定偏移量，长度（如果数组长度小于count 用0填充）
        /// </summary>
        public BytesEncode AddBytes(byte[] data, int offset, int count, bool convertLH = false)
        {
            if (convertLH)
            {
                data = BytesTools.Reverse(data);
            }
            for (int i = offset; i < offset + count; i++)
            {
                if (i < data.Length)
                {
                    AddByte(data[i]);
                }
                else
                {
                    AddByte(0);
                }
            }
            return this;
        }
        /// <summary>
        /// 设置指定位置的值
        /// </summary>
        public BytesEncode SetValue(int index, byte data)
        {
            _ReaultList[index] = data;
            return this;
        }
        /// <summary>
        /// 增加一组数组
        /// </summary>
        public BytesEncode AddBytes(byte[] data, bool convertLH)
        {
            return AddBytes(data, 0, data.Length, convertLH);
        }
        /// <summary>
        /// 增加2字节无符号数值 (默认低字节在前)
        /// </summary>
        public BytesEncode AddUInt16(UInt16 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加2字节有符号数值
        /// </summary>
        public BytesEncode AddInt16(Int16 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加4字节无符号数值
        /// </summary>
        public BytesEncode AddUInt32(UInt32 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加4字节有符号数值
        /// </summary>
        public BytesEncode AddInt32(Int32 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加8字节无符号数值
        /// </summary>
        public BytesEncode AddUInt64(UInt64 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加8字节有符号数值
        /// </summary>
        public BytesEncode AddInt64(Int64 data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加8字节 Double
        /// </summary>
        public BytesEncode AddDouble(double data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加4字节 Float
        /// </summary>
        public BytesEncode AddFloat(float data, bool convertLH = false)
        {
            var bs = BitConverter.GetBytes(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加16进制字符串
        /// </summary>
        public BytesEncode AddHex(string data, bool convertLH = false)
        {
            var bs = BytesTools.HexToByteArr(data);
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加指定编码的字符串
        /// </summary>
        /// <param name="data">要增加的数据</param>
        /// <param name="encode">编码</param>
        /// <param name="fiexdLength">固定长度</param>
        /// <param name="fillData">固定长度时，不足则填充的字符，没人为0</param>
        /// <param name="fillToLast">是否填充到尾部，默认时后面补填充</param>
        public BytesEncode AddString(string data, Encoding encode, int fiexdLength, byte fillData = 0, bool fillToLast = true)
        {
            var bs = encode.GetBytes(data);
            if (bs.Length < fiexdLength)
            {
                if (!fillToLast) AddBytes(bs);
                for (int i = 0; i < fiexdLength - bs.Length; i++)
                {
                    AddByte(fillData);
                }
                if (fillToLast) AddBytes(bs);
            }
            else
            {
                return AddBytes(bs, 0, fiexdLength);
            }

            if (fiexdLength <= 0) fiexdLength = bs.Length;
            return AddBytes(bs, 0, fiexdLength);
        }
        /// <summary>
        /// 增加一个用 UTF-8 解析的字符串，指定固定长度，不足尾部补空格
        /// </summary>
        /// <param name="data">要加入的字符串</param>
        /// <param name="fixedLength">最长字节数</param>
        public BytesEncode AddString(string data, int fixedLength, char fillStr)
        {
            return AddString(data, TextEncoding, fixedLength, (byte)fillStr);
        }
        /// <summary>
        /// GBK编码字符串，指定固定长度，不足补空格或指定字符
        /// </summary>
        /// <param name="data">要加入的字符串</param>
        /// <param name="fixedLength">最长字节数</param>
        /// <param name="fillToLast">是否填充到尾部</param>
        public BytesEncode AddString(string data, int fixedLength, char fillStr,bool fillToLast = true)
        {
            return AddString(data, TextEncoding, fixedLength, (byte)fillStr, fillToLast);
        }
        /// <summary>
        /// 增加一个用 UTF-8 解析的字符串，指定固定长度，不足前面补0
        /// </summary>
        /// <param name="data">要加入的字符串</param>
        /// <param name="fixedLength">最长字节数</param>
        public BytesEncode AddString(string data, int fixedLength)
        {
            return AddString(data, TextEncoding, fixedLength);
        }
        /// <summary>
        /// 增加一个默认GBK编码的字符串
        /// </summary>
        public BytesEncode AddString(string data, Encoding encode)
        {
            return AddString(data, encode, 0);
        }
        /// <summary>
        /// 按默认GBK编码加入字符串
        /// </summary>
        public BytesEncode AddString(string data)
        {
            return AddString(data, Encoding.GetEncoding("gbk"), 0);
        }
        /// <summary>
        /// 增加一个 xxx.xxx.xxx.xxx 格式的IP地址，4字节
        /// </summary>
        public BytesEncode AddIpString(string ip, bool convertLH = false)
        {
            var bs = IPAddress.Parse(ip).GetAddressBytes();
            return AddBytes(bs, convertLH);
        }
        /// <summary>
        /// 增加 count 个 回车换行,
        /// </summary>
        public BytesEncode AddLine(int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                AddString("\r\n");
            }
            return this;
        }
    }
}