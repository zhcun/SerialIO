﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO
{
    public class SerialOption
    {
        /// <summary>
        /// 串口名（COM1、COM2）
        /// </summary>
        public string PortName { set; get; }
        /// <summary>
        /// 波特率，默认 9600
        /// </summary>
        public int BaudRate { set; get; }
        /// <summary>
        /// 接收数据的缓存大小，默认 10240
        /// </summary>
        public int BufferSize { set; get; } = 10240;
        /// <summary>
        /// 一次读串口间隔毫秒，默认20毫秒
        /// </summary>
        public int ReadInterval { set; get; } = 10;
        /// <summary>
        /// 当发送需要排队时的间隔毫秒，所有发送数据将排队发送，当为 0 时不排队 立即发送, 默认20毫秒
        /// </summary>
        public int WriteInterval { set; get; } = 20;
    }
}