﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO.Event
{
    public class ReadWriteEvent
    {
        /// <summary>
        /// 读写状态，1:读，2：写
        /// </summary>
        public int Status { set; get; }

        public int Offset { set; get; }

        public int Count { set; get; }

        public byte[] Data { set; get; } 
    }
}