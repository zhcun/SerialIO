﻿using System;

namespace ZhCun.SerialIO.Event
{
    public class ConnectChangedEvent
    {
        /// <summary>
        /// 当前串口服务
        /// </summary>
        public ISerialServer Server { set; get; }
        /// <summary>
        /// 状态，1：打开，2关闭，3 异常
        /// </summary>
        public int Status { set; get; }
        /// <summary>
        /// 当连接异常时返回的异常消息
        /// </summary>
        public Exception ErrException { set; get; }
    }
}