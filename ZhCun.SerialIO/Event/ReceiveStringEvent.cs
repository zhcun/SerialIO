﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO.Event
{
    public class ReceiveStringEvent
    {
        public ISerialServer Server { set; get; }

        public string Data { set; get; }
    }
}
