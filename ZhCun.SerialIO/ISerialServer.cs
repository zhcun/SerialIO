﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.SerialIO.Event;

namespace ZhCun.SerialIO
{
    public interface ISerialServer : IDisposable
    {
        bool IsOpen { get; }

        /// <summary>
        /// 当连接状态改变时触发
        /// </summary>
        event Action<ConnectChangedEvent> ConnectChanged;
        /// <summary>
        /// 当读数据完成时触发
        /// </summary>
        event Action<ReadWriteEvent> DataReadOver;
        /// <summary>
        /// 当下发数据时触发
        /// </summary>
        event Action<ReadWriteEvent> DataWriteOver;
        /// <summary>
        /// 当前服务的配置
        /// </summary>
        SerialOption Option { get; }

        void Write(byte[] data);

        void Write(byte[] data, int offset, int count);

        //void Write(byte[] data, int offset, int count, int sendTimes, int interval);
        /// <summary>
        /// model对象发送数据
        /// </summary>
        /// <param name="model">model实体对象</param>
        /// <param name="level">级别，0: 立即发送，1：低优先级队列发送，2：中优先级队列发送，3：高优先级队列发送</param>
        void Write(IWriteModel model, int level = 0);

        void Write(IWriteModel model);
        /// <summary>
        /// 开始监听串口
        /// </summary>
        void Start(SerialOption option);
        /// <summary>
        /// 默认参数及指定串口开始服务
        /// </summary>
        void Start(string portName, int baudRate = 9600);
    }
}