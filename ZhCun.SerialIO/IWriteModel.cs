﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO
{
    /// <summary>
    /// 用于发送数据实体对象的接口
    /// </summary>
    public interface IWriteModel
    {
        byte[] GetData();
    }
}