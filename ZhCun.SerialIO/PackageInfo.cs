﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO
{
    public class PackageInfo
    {
        public PackageInfo(int key, byte[] data, int offset, int count)
        {
            Key = key;
            Body = data;
            BodyOffset = offset;
            BodyCount = count;
        }
        /// <summary>
        /// 数据
        /// </summary>
        public byte[] Body { get; private set; }
        /// <summary>
        /// 偏移量
        /// </summary>
        public int BodyOffset { get; private set; }
        /// <summary>
        /// 数据长度
        /// </summary>
        public int BodyCount { get; private set; }
        /// <summary>
        /// 成功后的key值，错误后小于0
        /// </summary>
        public int Key { set; get; } = -1;
        /// <summary>
        /// 当前包的状态
        /// 0：正常，
        /// 1：未找到包头丢弃，
        /// 2: 错误的长度
        /// </summary>
        public int Status { set; get; }
    }
}