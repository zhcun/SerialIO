﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO.Filters
{
    public interface IReceiveFilter
    {
        /// <summary>
        /// 解析过滤完成后触发的事件
        /// </summary>
        Action<PackageInfo> OnFilterFinish { set; get; }
        /// <summary>
        /// 解析器过滤
        /// </summary>
        /// <param name="recBuffer">用于半包缓存对象</param>
        /// <param name="data">当前接收的数据</param>
        /// <param name="offset">数据偏移量</param>
        /// <param name="count">数据长度</param>
        /// <param name="isBuffer">是否为缓存中获取(本地)</param>
        /// <param name="rest">剩余为解析字节数</param>
        void Filter(IBufferReceive recBuffer, byte[] data, int offset, int count, bool isBuffer, out int rest);        
    }
}