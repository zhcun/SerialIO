﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZhCun.SerialIO.Utils;

namespace ZhCun.SerialIO.Filters
{
    /// <summary>
    /// 固定头过滤器
    /// </summary>
    public abstract class FixedHeadFilter : ReceiveBaseFilter
    {
        public FixedHeadFilter(byte[] headBytes, int minLength)
        {
            HeadBytes = headBytes;
            MinLength = minLength;
        }

        byte[] HeadBytes { get; }

        protected internal override int MinLength { get; }
               
        protected internal override bool FindHead(byte[] data, int offset, int count, out int headOffset)
        {
            headOffset = BytesTools.FindBytes(data, offset, count, HeadBytes);
            return headOffset >= 0;
        }
    }
}