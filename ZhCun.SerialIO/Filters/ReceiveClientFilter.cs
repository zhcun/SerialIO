﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO.Filters
{
    /// <summary>
    /// 默认无任何规则的过滤，用于纯com工具的，客户端测试
    /// </summary>
    public class ReceiveClientFilter : ReceiveBaseFilter
    {
        protected internal override int MinLength => 1;

        public override void Filter(IBufferReceive recBuffer, byte[] data, int offset, int count, bool isBuffer, out int rest)
        {
            rest = 0;
        }

        protected internal override bool FindHead(byte[] data, int offset, int count, out int headOffset)
        {
            throw new NotImplementedException();
        }

        protected internal override int GetDataLength(byte[] data, int offset, int count)
        {
            throw new NotImplementedException();
        }

        protected internal override int GetPackageKey(byte[] data, int offset, int count)
        {
            throw new NotImplementedException();
        }
    }
}