﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO
{
    public interface IBufferReceive
    {
        /// <summary>
        /// 接收缓存大小，默认为1024（如需更改在子类够创造函数中更改）
        /// </summary>
        //int ReceiveBufferLength { get; }
        /// <summary>
        /// 缓存偏移量
        /// </summary>
        int ReceiveOffset { get; }
        /// <summary>
        /// 接收缓冲区
        /// </summary>
        byte[] ReceiveBuffer { get; }
        /// <summary>
        /// 当需要分包或粘包数据时，设置接收缓存
        /// </summary>
        int SetReceiveBuffer(byte[] data, int offset, int count);
        /// <summary>
        /// 判断是否有接收缓存数据，判断偏移量
        /// </summary>
        bool HasReceiveBuffer();
        /// <summary>
        /// 重置缓存偏移量
        /// </summary>
        void RestReceiveBuffer();
    }
}
