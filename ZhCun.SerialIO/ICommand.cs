﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialIO
{
    public interface ICommand
    {
        int Key { get; }

        void Execute(ISerialServer server, PackageInfo package);
    }
}