﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ZhCun.SerialDebug
{
    public partial class ucDataSend : UserControl, ISendParameter
    {
        public ucDataSend()
        {
            InitializeComponent();
        }

        public static ISendParameter Create()
        {
            return new ucDataSend();
        }

        public event Action<ISendParameter> OnSendData;

        public int Index { set; get; }

        public bool IsSelected
        {
            get { return chBoxSelected.Checked; }
            set { chBoxSelected.Checked = value; }
        }

        public string Caption
        {
            get { return txtCaption.Text; }
            set { txtCaption.Text = value; }
        }

        public string Data
        {
            get { return txtData.Text; }
            set { txtData.Text = value; }
        }

        public int IntervalMS
        {
            get { return int.Parse(txtMS.Text); }
            set { txtMS.Text = value.ToString(); }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            OnSendData?.Invoke(this);
        }
    }

    public interface ISendParameter
    {
        event Action<ISendParameter> OnSendData;

        int Index { set; get; }

        bool IsSelected { set; get; }

        string Caption { set; get; }

        string Data { set; get; }

        int IntervalMS { set; get; }

    }
}
