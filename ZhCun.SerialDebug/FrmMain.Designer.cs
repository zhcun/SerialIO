﻿
namespace ZhCun.SerialDebug
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.coBoxPorts = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSendExtent = new System.Windows.Forms.Button();
            this.btnOpenPort = new System.Windows.Forms.Button();
            this.txtBaudRate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSendData = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLoopSec = new System.Windows.Forms.TextBox();
            this.chBoxLoopSend = new System.Windows.Forms.CheckBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.ucLogView1 = new ZhCun.Win.ucLogView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "串口：";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 573);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1007, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // coBoxPorts
            // 
            this.coBoxPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxPorts.FormattingEnabled = true;
            this.coBoxPorts.Location = new System.Drawing.Point(74, 18);
            this.coBoxPorts.Name = "coBoxPorts";
            this.coBoxPorts.Size = new System.Drawing.Size(121, 22);
            this.coBoxPorts.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSendExtent);
            this.panel1.Controls.Add(this.btnOpenPort);
            this.panel1.Controls.Add(this.txtBaudRate);
            this.panel1.Controls.Add(this.coBoxPorts);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1007, 60);
            this.panel1.TabIndex = 3;
            // 
            // btnSendExtent
            // 
            this.btnSendExtent.Location = new System.Drawing.Point(485, 14);
            this.btnSendExtent.Name = "btnSendExtent";
            this.btnSendExtent.Size = new System.Drawing.Size(96, 29);
            this.btnSendExtent.TabIndex = 5;
            this.btnSendExtent.Text = "扩展发送";
            this.btnSendExtent.UseVisualStyleBackColor = true;
            this.btnSendExtent.Click += new System.EventHandler(this.btnSendExtent_Click);
            // 
            // btnOpenPort
            // 
            this.btnOpenPort.Location = new System.Drawing.Point(376, 14);
            this.btnOpenPort.Name = "btnOpenPort";
            this.btnOpenPort.Size = new System.Drawing.Size(83, 29);
            this.btnOpenPort.TabIndex = 4;
            this.btnOpenPort.Text = "打开串口";
            this.btnOpenPort.UseVisualStyleBackColor = true;
            this.btnOpenPort.Click += new System.EventHandler(this.btnOpenPort_Click);
            // 
            // txtBaudRate
            // 
            this.txtBaudRate.Location = new System.Drawing.Point(273, 18);
            this.txtBaudRate.Name = "txtBaudRate";
            this.txtBaudRate.Size = new System.Drawing.Size(97, 23);
            this.txtBaudRate.TabIndex = 3;
            this.txtBaudRate.Text = "9600";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(214, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "波特率：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSendData);
            this.panel2.Controls.Add(this.btnSend);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1007, 72);
            this.panel2.TabIndex = 4;
            // 
            // txtSendData
            // 
            this.txtSendData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSendData.Location = new System.Drawing.Point(300, 0);
            this.txtSendData.Multiline = true;
            this.txtSendData.Name = "txtSendData";
            this.txtSendData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSendData.Size = new System.Drawing.Size(707, 72);
            this.txtSendData.TabIndex = 1;
            this.txtSendData.TabStop = false;
            // 
            // btnSend
            // 
            this.btnSend.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSend.Location = new System.Drawing.Point(216, 0);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(84, 72);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "发送";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtLoopSec);
            this.panel3.Controls.Add(this.chBoxLoopSend);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(216, 72);
            this.panel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(155, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "ms/次";
            // 
            // txtLoopSec
            // 
            this.txtLoopSec.Location = new System.Drawing.Point(100, 8);
            this.txtLoopSec.Name = "txtLoopSec";
            this.txtLoopSec.Size = new System.Drawing.Size(51, 23);
            this.txtLoopSec.TabIndex = 1;
            this.txtLoopSec.Text = "1000";
            // 
            // chBoxLoopSend
            // 
            this.chBoxLoopSend.AutoSize = true;
            this.chBoxLoopSend.Location = new System.Drawing.Point(12, 10);
            this.chBoxLoopSend.Name = "chBoxLoopSend";
            this.chBoxLoopSend.Size = new System.Drawing.Size(82, 18);
            this.chBoxLoopSend.TabIndex = 0;
            this.chBoxLoopSend.Text = "循环发送";
            this.chBoxLoopSend.UseVisualStyleBackColor = true;
            this.chBoxLoopSend.CheckedChanged += new System.EventHandler(this.chBoxLoopSend_CheckedChanged);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 132);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1007, 3);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // ucLogView1
            // 
            this.ucLogView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucLogView1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucLogView1.Location = new System.Drawing.Point(0, 135);
            this.ucLogView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ucLogView1.MaxRow = 500;
            this.ucLogView1.Name = "ucLogView1";
            this.ucLogView1.Size = new System.Drawing.Size(1007, 438);
            this.ucLogView1.TabIndex = 5;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 595);
            this.Controls.Add(this.ucLogView1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "串口调试工具  开源：https://gitee.com/zhcun/SerialIO";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ComboBox coBoxPorts;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBaudRate;
        private System.Windows.Forms.Button btnOpenPort;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtSendData;
        private Win.ucLogView ucLogView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chBoxLoopSend;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLoopSec;
        private System.Windows.Forms.Button btnSendExtent;
    }
}

