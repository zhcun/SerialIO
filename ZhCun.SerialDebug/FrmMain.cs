﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZhCun.Utils.Helpers;

namespace ZhCun.SerialDebug
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        Config ConfigObj { get; } = Config.Create();

        static byte[] ReceiveBuffer { get; } = new byte[1024];

        SerialPort Serial { get; } = new SerialPort();

        void PrintLog(string log)
        {
            ucLogView1.AddLog(log);
        }

        void WriteData(string dataHex)
        {
            var data = BytesHelper.HexToByteArr(dataHex);
            if (!Serial.IsOpen) return;
            Serial.Write(data, 0, data.Length);
            PrintLog($"[W]: {dataHex}");
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            var comPorts = SerialPort.GetPortNames();
            coBoxPorts.Items.Clear();
            if (comPorts != null)
            {
                foreach (var item in comPorts)
                {
                    coBoxPorts.Items.Add(item);
                }
            }

            coBoxPorts.Text = ConfigObj.ComNo;
            txtBaudRate.Text = ConfigObj.BaudRate.ToString();
            txtSendData.Text = ConfigObj.DefaultSend;
            //string comNo = Xml.Read(ConfigFile, "main", "com");
            //coBoxPorts.Text = comNo;
            //txtBaudRate.Text = Xml.Read(ConfigFile, "main", "BaudRate");

            Serial.DataReceived += Serial_DataReceived;
            Serial.ErrorReceived += Serial_ErrorReceived;
            Serial.PinChanged += Serial_PinChanged;
            Serial.Disposed += Serial_Disposed;
        }

        private void Serial_Disposed(object sender, EventArgs e)
        {
            PrintLog("Serial Disposed");
        }

        private void Serial_PinChanged(object sender, SerialPinChangedEventArgs e)
        {
            PrintLog($"Serial PinChanged,  EventType:{ e.EventType}");
        }

        private void Serial_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            PrintLog($"Serial ErrorReceived,  EventType:{ e.EventType}");
        }

        private void Serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int offset = 0;
            while (Serial.BytesToRead > 0)
            {
                if (offset + Serial.BytesToRead > ReceiveBuffer.Length) break;
                offset += Serial.Read(ReceiveBuffer, offset, Serial.BytesToRead);
                Thread.Sleep(30);
            }
            var data = BytesHelper.ByteArrToHex(ReceiveBuffer, 0, offset);
            PrintLog($"[R]: {data}");
        }

        private void btnOpenPort_Click(object sender, EventArgs e)
        {
            if (btnOpenPort.Text == "打开串口")
            {
                Serial.PortName = coBoxPorts.Text;
                Serial.BaudRate = int.Parse(txtBaudRate.Text);
                Serial.Open();
                btnOpenPort.Text = "关闭串口";
            }
            else
            {
                Serial.DiscardInBuffer();
                Serial.DiscardOutBuffer();
                Serial.Close();
                Serial.Dispose();
                btnOpenPort.Text = "打开串口";
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Task.Factory.StartNew(LoopSend);
        }

        void LoopSend()
        {
            int interval = int.Parse(txtLoopSec.Text);
            do
            {
                try
                {
                    WriteData(txtSendData.Text);
                }
                catch (Exception ex)
                {
                    PrintLog($"发送数据发生异常：{ex.Message}");
                }
                if (!chBoxLoopSend.Checked) break;
                Thread.Sleep(interval);
            }
            while (chBoxLoopSend.Checked);
        }


        private void chBoxLoopSend_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSendExtent_Click(object sender, EventArgs e)
        {
            FrmSendExtent frm = FrmSendExtent.Create(WriteData);
            frm.TopLevel = true;
            frm.Show();            
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            ConfigObj.ComNo = coBoxPorts.Text;
            if (int.TryParse(txtBaudRate.Text, out int baudRate))
            {
                ConfigObj.BaudRate = baudRate;
            }
            Config.SaveConfig(ConfigObj);
        }
    }
}