﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ZhCun.SerialDebug
{
    class Config
    {
        static string ConfigFile = "Config.json";

        static Config Instance { set; get; }

        public static Config Create()
        {
            if (Instance == null)
            {
                var json = File.ReadAllText(ConfigFile);
                Instance = JsonHelper.JsonToObject<Config>(json);
            }
            return Instance;
        }

        public static void SaveConfig(Config config)
        {
            var json = JsonHelper.ToJson(config);
            using (StreamWriter sw = new StreamWriter(ConfigFile, false))
            {
                sw.Write(json);
            }
        }

        public string ComNo { set; get; }

        public int BaudRate { set; get; }

        public string DefaultSend { set; get; }

        public ConfigData[] DataRows { set; get; }
    }

    class ConfigData
    {
        public bool IsSelected { set; get; }

        public string Caption { set; get; }

        public int IntervalMS { set; get; }

        public string Data { set; get; }
    }
}