﻿
namespace ZhCun.SerialDebug
{
    partial class FrmSendExtent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chBoxLoopSend = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.coBoxSelected = new System.Windows.Forms.ComboBox();
            this.panelData = new System.Windows.Forms.Panel();
            this.ucDataSend1 = new ZhCun.SerialDebug.ucDataSend();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelData.SuspendLayout();
            this.SuspendLayout();
            // 
            // chBoxLoopSend
            // 
            this.chBoxLoopSend.AutoSize = true;
            this.chBoxLoopSend.Location = new System.Drawing.Point(12, 12);
            this.chBoxLoopSend.Name = "chBoxLoopSend";
            this.chBoxLoopSend.Size = new System.Drawing.Size(82, 18);
            this.chBoxLoopSend.TabIndex = 0;
            this.chBoxLoopSend.Text = "循环发送";
            this.chBoxLoopSend.UseVisualStyleBackColor = true;
            this.chBoxLoopSend.CheckedChanged += new System.EventHandler(this.chBoxLoopSend_CheckedChanged);
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(93, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "说明";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(196, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "延时(ms)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(343, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(236, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "数据";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chBoxLoopSend);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(584, 39);
            this.panel2.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.coBoxSelected);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 39);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(584, 32);
            this.panel1.TabIndex = 4;
            // 
            // coBoxSelected
            // 
            this.coBoxSelected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxSelected.FormattingEnabled = true;
            this.coBoxSelected.Items.AddRange(new object[] {
            "默认",
            "全选",
            "反选",
            "全不选"});
            this.coBoxSelected.Location = new System.Drawing.Point(3, 4);
            this.coBoxSelected.Name = "coBoxSelected";
            this.coBoxSelected.Size = new System.Drawing.Size(70, 22);
            this.coBoxSelected.TabIndex = 3;
            this.coBoxSelected.SelectedIndexChanged += new System.EventHandler(this.coBoxSelected_SelectedIndexChanged);
            // 
            // panelData
            // 
            this.panelData.Controls.Add(this.ucDataSend1);
            this.panelData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelData.Location = new System.Drawing.Point(0, 71);
            this.panelData.Name = "panelData";
            this.panelData.Size = new System.Drawing.Size(584, 391);
            this.panelData.TabIndex = 7;
            // 
            // ucDataSend1
            // 
            this.ucDataSend1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ucDataSend1.Caption = "行1";
            this.ucDataSend1.Data = "AA BB CC DD 00 01 02 04 FF";
            this.ucDataSend1.Dock = System.Windows.Forms.DockStyle.Top;
            this.ucDataSend1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ucDataSend1.Index = 0;
            this.ucDataSend1.IntervalMS = 100;
            this.ucDataSend1.IsSelected = true;
            this.ucDataSend1.Location = new System.Drawing.Point(0, 0);
            this.ucDataSend1.Name = "ucDataSend1";
            this.ucDataSend1.Size = new System.Drawing.Size(584, 30);
            this.ucDataSend1.TabIndex = 0;
            // 
            // FrmSendExtent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.panelData);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSendExtent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "发送扩展";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSendExtent_FormClosing);
            this.Load += new System.EventHandler(this.FrmSendExtent_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panelData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chBoxLoopSend;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelData;
        private System.Windows.Forms.ComboBox coBoxSelected;
        private ucDataSend ucDataSend1;
    }
}