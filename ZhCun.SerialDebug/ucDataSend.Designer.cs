﻿
namespace ZhCun.SerialDebug
{
    partial class ucDataSend
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.txtMS = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.txtCaption = new System.Windows.Forms.TextBox();
            this.chBoxSelected = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(266, 2);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(69, 23);
            this.btnSend.TabIndex = 7;
            this.btnSend.Text = "发送";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtMS
            // 
            this.txtMS.Location = new System.Drawing.Point(200, 3);
            this.txtMS.Name = "txtMS";
            this.txtMS.Size = new System.Drawing.Size(60, 23);
            this.txtMS.TabIndex = 4;
            this.txtMS.Text = "100";
            this.txtMS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtData
            // 
            this.txtData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtData.Location = new System.Drawing.Point(341, 3);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(388, 23);
            this.txtData.TabIndex = 5;
            this.txtData.Text = "AA BB CC DD 00 01 02 04 FF";
            // 
            // txtCaption
            // 
            this.txtCaption.Location = new System.Drawing.Point(84, 3);
            this.txtCaption.Name = "txtCaption";
            this.txtCaption.Size = new System.Drawing.Size(112, 23);
            this.txtCaption.TabIndex = 6;
            this.txtCaption.Text = "行1";
            this.txtCaption.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chBoxSelected
            // 
            this.chBoxSelected.AutoSize = true;
            this.chBoxSelected.Checked = true;
            this.chBoxSelected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chBoxSelected.Location = new System.Drawing.Point(12, 6);
            this.chBoxSelected.Name = "chBoxSelected";
            this.chBoxSelected.Size = new System.Drawing.Size(54, 18);
            this.chBoxSelected.TabIndex = 8;
            this.chBoxSelected.Text = "选择";
            this.chBoxSelected.UseVisualStyleBackColor = true;
            // 
            // ucDataSend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.txtCaption);
            this.Controls.Add(this.chBoxSelected);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtMS);
            this.Controls.Add(this.txtData);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "ucDataSend";
            this.Size = new System.Drawing.Size(732, 30);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtMS;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.TextBox txtCaption;
        private System.Windows.Forms.CheckBox chBoxSelected;
    }
}
