﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using ZhCun.Utils.Helpers;

namespace ZhCun.SerialDebug
{
    public partial class FrmSendExtent : Form
    {
        private FrmSendExtent()
        {
            InitializeComponent();
        }

        //const string ConfigFile = "Config.xml";

        static FrmSendExtent _Instance;

        public static FrmSendExtent Create(Action<string> OnSendData)
        {
            if (_Instance == null || _Instance.IsDisposed)
            {
                _Instance = new FrmSendExtent();
                _Instance.SendDataAction = OnSendData;
            }
            return _Instance;
        }

        Config ConfigObj { get; } = Config.Create();

        Action<string> SendDataAction;

        ucDataSend CreateDataControl(ConfigData data,int index)
        {
            ucDataSend uc = new ucDataSend();
            uc.OnSendData += Uc_OnSendData;
            uc.Caption = data.Caption;
            uc.IsSelected = data.IsSelected;
            uc.Data = data.Data;
            uc.IntervalMS = data.IntervalMS;
            uc.Index = index;
            uc.Dock = DockStyle.Top;
            return uc;
        }

        void LoadDataControl()
        {
            panelData.Controls.Clear();
            for (int i = 0; i < ConfigObj.DataRows.Length; i++)
            {
                var data = ConfigObj.DataRows[i];
                ucDataSend uc = CreateDataControl(data, i);                
                panelData.Controls.Add(uc);
                uc.BringToFront();
            }

            //panelData.Controls.Clear();
            //XmlDocument doc = new XmlDocument();
            //doc.Load(ConfigFile);
            //var nodeList = doc.SelectNodes($"Root/data/row");

            //for (int i = 0; i < nodeList.Count; i++)
            //{
            //    XmlNode node = nodeList[i];
            //    ucDataSend uc = new ucDataSend();
            //    uc.OnSendData += Uc_OnSendData;
            //    uc.Caption = node.Attributes["Caption"]?.Value;
            //    uc.IsSelected = bool.Parse(node.Attributes["IsSelected"]?.Value);
            //    uc.Data = node.Attributes["Data"]?.Value;
            //    uc.IntervalMS = int.Parse(node.Attributes["IntervalMS"].Value);
            //    uc.Index = i; //int.Parse(node.Attributes["Index"].Value);
            //    //uc.SendToBack();
            //    uc.Dock = DockStyle.Top;
            //    panelData.Controls.Add(uc);

            //    uc.BringToFront();
            //}
        }

        private void FrmSendExtent_Load(object sender, EventArgs e)
        {
            LoadDataControl();
            coBoxSelected.SelectedIndex = 0;
        }

        private void Uc_OnSendData(ISendParameter arg)
        {
            SendDataAction(arg.Data);
        }

        private void chBoxLoopSend_CheckedChanged(object sender, EventArgs e)
        {
            if (chBoxLoopSend.Checked)
            {
                Task.Factory.StartNew(LoopSendHandle);
                void LoopSendHandle()
                {
                    while (chBoxLoopSend.Checked)
                    {
                        foreach (ISendParameter item in panelData.Controls)
                        {
                            SendDataAction(item.Data);
                            Thread.Sleep(item.IntervalMS);
                        }
                    }
                }
            }
        }

        private void FrmSendExtent_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (ISendParameter item in panelData.Controls)
            {
                var data = ConfigObj.DataRows[item.Index];
                data.Caption = item.Caption;
                data.Data = item.Data;
                data.IntervalMS = item.IntervalMS;
                data.IsSelected = item.IsSelected;
            }
        }

        private void coBoxSelected_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (coBoxSelected.SelectedIndex == 0)
            {
                return;
            }
            if (coBoxSelected.SelectedIndex == 1)
            {
                foreach (ISendParameter item in panelData.Controls)
                {
                    item.IsSelected = true;
                }
            }
            else if (coBoxSelected.SelectedIndex == 2)
            {
                foreach (ISendParameter item in panelData.Controls)
                {
                    item.IsSelected = !item.IsSelected;
                }
            }
            else if (coBoxSelected.SelectedIndex == 3)
            {
                foreach (ISendParameter item in panelData.Controls)
                {
                    item.IsSelected = false;
                }
            }
        }
    }
}