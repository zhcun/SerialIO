﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZhCun.SerialDebug
{
    class JsonHelper
    {
        static JsonHelper()
        {
            JsonSettring = new JsonSerializerSettings
            {
                DateFormatString = "yyyy-MM-dd HH:mm:ss",
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        readonly static JsonSerializerSettings JsonSettring;
        /// <summary>
        /// 将指定对象序列化json字符串
        /// </summary>
        public static string ToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, JsonSettring);
        }
        /// <summary>
        /// 将json字符串转成对象
        /// </summary>
        public static object JsonToObject(string data)
        {
            return JsonConvert.DeserializeObject(data, JsonSettring);
        }
        /// <summary>
        /// 通过json字符串序列化成对象
        /// </summary>
        public static T JsonToObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data, JsonSettring);
        }
    }
}