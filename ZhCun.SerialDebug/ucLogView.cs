﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZhCun.Win
{
    public partial class ucLogView : UserControl
    {
        public ucLogView()
        {
            InitializeComponent();
        }

        string LastLog { set; get; }

        int LogTimes { set; get; } = 0;

        /// <summary>
        /// 最大行数，默认为500
        /// </summary>
        public int MaxRow { set; get; } = 500;

        public void AddLog(string log)
        {
            if (this.Parent != null && Parent.IsDisposed || Parent.Disposing)
            {
                Console.WriteLine($"已释放容器窗体写入日志，退出，Log:{log}");
                return;
            }
            if (this.Disposing || this.IsDisposed || lvLog.IsDisposed || lvLog.Disposing)
            {
                //LogHelper.LogObj.Info($"已释放对象写入日志，退出，Log:{log}");
                Console.WriteLine($"已释放对象写入日志，退出，Log:{log}");
                return;
            }
            if (this.InvokeRequired)
            {
                Action<string> act = AddLog;
                lvLog.Invoke(act, log);
            }
            else
            {
                if (log == LastLog && lvLog.Items.Count > 0)
                {
                    LogTimes++;
                }
                else
                {
                    LogTimes = 1;
                    LastLog = log;
                }
                if (LogTimes > 2)
                {
                    lvLog.Items[0].SubItems[0].Text = $"[{DateTime.Now:HH:mm:ss.fff}]";
                    lvLog.Items[0].SubItems[1].Text = $"[{LogTimes}]";
                }
                else
                {

                    if (lvLog.IsDisposed) return;
                    var logItem = lvLog.Items.Insert(0, $"[{DateTime.Now:HH:mm:ss.fff}]");
                    logItem.SubItems.Add($"[{LogTimes}]");
                    logItem.SubItems.Add(log);
                }

                if (lvLog.Items.Count > MaxRow)
                {
                    lvLog.Items.RemoveAt(MaxRow - 1);
                }
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            lvLog.Items.Clear();
            Task.Factory.StartNew(() =>
            {
                Action task = () =>
                {
                    Thread.Sleep(500);
                    chContent.Width = lvLog.Width - chTime.Width - chCount.Width - 5;
                };
                lvLog.Invoke(task);
            });
        }

        private void lvLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvLog.SelectedItems.Count == 0) return;
            var c1 = lvLog.SelectedItems[0].SubItems[0].Text;
            var c2 = lvLog.SelectedItems[0].SubItems[1].Text;
            var c3 = lvLog.SelectedItems[0].SubItems[2].Text;

            txtEdit.Text = $"{c1} {c2} {c3}";
        }

        private void tsmiCopyData_Click(object sender, EventArgs e)
        {
            if (lvLog.SelectedItems?.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (ListViewItem item in lvLog.SelectedItems)
                {
                    for (int i = 0; i < item.SubItems.Count; i++)
                    {
                        sb.Append($"{item.SubItems[i].Text} ");
                    }
                    sb.Append("\r\n");
                }
                Clipboard.SetDataObject(sb.ToString());
                MessageBox.Show("已复制到剪切板");
            }
        }

        private void tsmiClear_Click(object sender, EventArgs e)
        {
            lvLog.Items.Clear();
            LastLog = null;
            txtEdit.Text = string.Empty;
        }

        private void tsmiViewEdit_Click(object sender, EventArgs e)
        {
            if (txtEdit.Visible)
            {
                tsmiViewEdit.Text = "显示编辑框";
                txtEdit.Visible = false;
            }
            else
            {
                tsmiViewEdit.Text = "隐藏编辑框";
                txtEdit.Visible = true;
            }
        }

        private void tsAllSelect_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvLog.Items)
            {
                item.Selected = true;
            }
        }
    }
}