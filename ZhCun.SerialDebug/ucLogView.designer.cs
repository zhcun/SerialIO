﻿
namespace ZhCun.Win
{
    partial class ucLogView
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "22:59:59.123",
            "9999",
            "0000000000000000000000000000000000000000000000000000000000"}, -1);
            this.lvLog = new System.Windows.Forms.ListView();
            this.chTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chContent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCopyData = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiClear = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiViewEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.txtEdit = new System.Windows.Forms.TextBox();
            this.tsAllSelect = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lvLog
            // 
            this.lvLog.BackColor = System.Drawing.Color.White;
            this.lvLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTime,
            this.chCount,
            this.chContent});
            this.lvLog.ContextMenuStrip = this.cmsMenu;
            this.lvLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvLog.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lvLog.FullRowSelect = true;
            this.lvLog.HideSelection = false;
            listViewItem1.StateImageIndex = 0;
            this.lvLog.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.lvLog.Location = new System.Drawing.Point(0, 26);
            this.lvLog.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lvLog.Name = "lvLog";
            this.lvLog.Size = new System.Drawing.Size(642, 269);
            this.lvLog.TabIndex = 12;
            this.lvLog.UseCompatibleStateImageBehavior = false;
            this.lvLog.View = System.Windows.Forms.View.Details;
            this.lvLog.SelectedIndexChanged += new System.EventHandler(this.lvLog_SelectedIndexChanged);
            // 
            // chTime
            // 
            this.chTime.Text = "时间";
            this.chTime.Width = 120;
            // 
            // chCount
            // 
            this.chCount.Text = "次数";
            this.chCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chContent
            // 
            this.chContent.Text = "日志";
            this.chContent.Width = 476;
            // 
            // cmsMenu
            // 
            this.cmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCopyData,
            this.tsmiClear,
            this.tsmiViewEdit,
            this.tsAllSelect});
            this.cmsMenu.Name = "cmsMenu";
            this.cmsMenu.Size = new System.Drawing.Size(181, 114);
            // 
            // tsmiCopyData
            // 
            this.tsmiCopyData.Name = "tsmiCopyData";
            this.tsmiCopyData.Size = new System.Drawing.Size(180, 22);
            this.tsmiCopyData.Text = "复制到剪切板";
            this.tsmiCopyData.Click += new System.EventHandler(this.tsmiCopyData_Click);
            // 
            // tsmiClear
            // 
            this.tsmiClear.Name = "tsmiClear";
            this.tsmiClear.Size = new System.Drawing.Size(180, 22);
            this.tsmiClear.Text = "清空";
            this.tsmiClear.Click += new System.EventHandler(this.tsmiClear_Click);
            // 
            // tsmiViewEdit
            // 
            this.tsmiViewEdit.Name = "tsmiViewEdit";
            this.tsmiViewEdit.Size = new System.Drawing.Size(180, 22);
            this.tsmiViewEdit.Text = "显示编辑框";
            this.tsmiViewEdit.Click += new System.EventHandler(this.tsmiViewEdit_Click);
            // 
            // txtEdit
            // 
            this.txtEdit.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtEdit.Location = new System.Drawing.Point(0, 0);
            this.txtEdit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtEdit.Name = "txtEdit";
            this.txtEdit.Size = new System.Drawing.Size(642, 26);
            this.txtEdit.TabIndex = 14;
            this.txtEdit.Visible = false;
            // 
            // tsAllSelect
            // 
            this.tsAllSelect.Name = "tsAllSelect";
            this.tsAllSelect.Size = new System.Drawing.Size(180, 22);
            this.tsAllSelect.Text = "全选";
            this.tsAllSelect.Click += new System.EventHandler(this.tsAllSelect_Click);
            // 
            // ucLogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lvLog);
            this.Controls.Add(this.txtEdit);
            this.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ucLogView";
            this.Size = new System.Drawing.Size(642, 295);
            this.cmsMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvLog;
        private System.Windows.Forms.ColumnHeader chTime;
        private System.Windows.Forms.ColumnHeader chContent;
        private System.Windows.Forms.ContextMenuStrip cmsMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiCopyData;
        private System.Windows.Forms.TextBox txtEdit;
        private System.Windows.Forms.ToolStripMenuItem tsmiClear;
        private System.Windows.Forms.ColumnHeader chCount;
        private System.Windows.Forms.ToolStripMenuItem tsmiViewEdit;
        private System.Windows.Forms.ToolStripMenuItem tsAllSelect;
    }
}
