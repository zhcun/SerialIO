# SerialIO

#### 介绍
一款串口通讯的封装，更容易的处理协议解析，内部实现了粘包、分包、冗余错误包的处理实现；
它可更方便的业务逻辑的处理，将通讯、协议解析、业务逻辑 完全独立开来，更容易扩展和修改代码逻辑；
本项目参考了SuperSocket中协议解析的设计原理，可外部 命令(Command)类， 对应协议中命令字灵活实现逻辑。


#### 软件架构
软件架构说明
当前项目仅有 .Net 4.0 版本

#### 固定头协议示例说明
当前Demo中仅测试了 固定包头 的协议解析；

固定头的协议示例代码

协议格式：[2字节 固定头 0xaa,0xbb] + [1字节 长度 len] [1字节 命令字] [1字节 设备号] [N字节 data] [2字节 CRC校验码]
PS： data长度 N = len - 7

下位机下发指令：
1. data为16进制数值
2. data为字符串内容

程序步骤：
1. 创建接收过滤器 FHDemoFilter 继承于 FixedHeadFilter
2. 创建接收的命令处理对象 CmdHex CmdText
3. 创建服务类 FDServer 将 FHDemoFilter 与 CmdHex、CmdText 加入到服务中
4. 创建协议对应的 接收数据Model R_Hex,R_Text 与 发送数据Model W_Hex,W_Text

#### 固定头协议测试窗体
![输入图片说明](https://images.gitee.com/uploads/images/2021/0929/194434_d556fbfd_382749.png "屏幕截图.png")

#### 其它
 希望有更多的使用，而完善更多种的通讯协议的解析；
 更多介绍：https://www.cnblogs.com/xtdhb/p/SerialIo.html

